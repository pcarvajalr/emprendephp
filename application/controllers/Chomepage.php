<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chomepage extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        $this->load->view('layout/headhome');
        $this->load->view('layout/navhome');
		$this->load->view('vhomepage');
        $this->load->view('layout/footerhome');
    }

    public function login(){
        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('vlogin');
        $this->load->view('layout/footer');
    }

    public function register(){
        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('vregister');
        $this->load->view('layout/footer');
    }

    public function forgotpass(){
        $this->load->view('layout/header');
        $this->load->view('layout/nav');
        $this->load->view('vforgotpass');
        $this->load->view('layout/footer');
    }
}