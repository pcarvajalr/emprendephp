<div class="ui container" id="register-form">
	<form action="php/crud/db_registred.php" method="POST" class="ui form">
  	<h2 class="ui dividing header pink">User Profile</h2>
	  	<div class="ui fields">
	  		<div class="twelve wide field">
	  			<div class="field">
	  			   	<label>e-mail</label>
	  			   	<div class="">
	  			   		<div class="sixteen wide field">
	  			       		<input type="email" required="required" name="txtEmail" placeholder="e-mail">
	  			   		</div>
	  			   	</div>
	  			</div>
	  			<div class="field">
	  			   	<label>Name</label>
	  				<div class="ui fields">
	  					<div class="eight wide field">
	  			   			<input type="text" required="required" name="txtName" placeholder="First Name">
	  					</div>
	  					<div class="eight wide field">
	  						<input type="text" required="required" name="txtLast" placeholder="Last Name">
	  			      	</div>
	  			    </div>
	  			</div>
	  		</div>
	  		<div class="four wide field">
	  			<div>
	  				<button class="ui button basic" type="file" size="25" width="40" name="usPicture">Picture</button>
	  			</div>
	  		</div>
	  	</div>
  		<div class="field">
  			<label for="password">Password</label>
  			<div>
  				<div>
  					<input type="password" required="required" name="usPassword">
  				</div>
  			</div>
  		</div>
  		<div class="field">
  			<label for="password">Confirm you password</label>
  			<div>
  				<div>
  					<input type="password" required="required" name="confirmPassword">
  				</div>
  			</div>
  		</div>
  		<div class="field">
  			<label for="languaje">Languaje</label>
  			<div>
  				<div>
  					<input type="text" required="required" name="txtLang">
  				</div>
  			</div>
  		</div>
  		<div class="two fields">
    		<div class="field">
    		<label>Country</label>
            <div class="ui fluid search selection dropdown">
  <input type="hidden" name="country">
  <i class="dropdown icon"></i>
  <div class="default text">Select Country</div>
  <div class="menu"> 
  <div class="item" data-value="ar"><i class="ar flag"></i>Argentina</div>
  <div class="item" data-value="au"><i class="au flag"></i>Australia</div>
  <div class="item" data-value="bo"><i class="bo flag"></i>Bolivia</div>
  <div class="item" data-value="br"><i class="br flag"></i>Brazil</div>
  <div class="item" data-value="ca"><i class="ca flag"></i>Canada</div>
  <div class="item" data-value="cl"><i class="cl flag"></i>Chile</div>
  <div class="item" data-value="cn"><i class="cn flag"></i>China</div>
  <div class="item" data-value="co"><i class="co flag"></i>Colombia</div>
  <div class="item" data-value="cr"><i class="cr flag"></i>Costa Rica</div>
  <div class="item" data-value="ec"><i class="ec flag"></i>Ecuador</div>
  <div class="item" data-value="gb"><i class="gb flag"></i>England</div>
  <div class="item" data-value="fr"><i class="fr flag"></i>France</div>
  <div class="item" data-value="de"><i class="de flag"></i>Germany</div>
  <div class="item" data-value="jp"><i class="jp flag"></i>Japan</div>
  <div class="item" data-value="mx"><i class="mx flag"></i>Mexico</div>
  <div class="item" data-value="pa"><i class="pa flag"></i>Panama</div>
  <div class="item" data-value="py"><i class="py flag"></i>Paraguay</div>
  <div class="item" data-value="pe"><i class="pe flag"></i>Peru</div>
  <div class="item" data-value="pt"><i class="pt flag"></i>Portugal</div>
  <div class="item" data-value="ru"><i class="ru flag"></i>Russia</div>
  <div class="item" data-value="sa"><i class="sa flag"></i>Saudi Arabia</div>
  <div class="item" data-value="za"><i class="za flag"></i>South Africa</div>
  <div class="item" data-value="kr"><i class="kr flag"></i>South Korea</div>
  <div class="item" data-value="es"><i class="es flag"></i>Spain</div>
  <div class="item" data-value="tw"><i class="tw flag"></i>Taiwan</div>
  <div class="item" data-value="th"><i class="th flag"></i>Thailand</div>
  <div class="item" data-value="us"><i class="us flag"></i>United States</div>
  <div class="item" data-value="ve"><i class="ve flag"></i>Venezuela</div>
</div>
 </div>
    		</div>
    	</div>
  		<div class="ui button inverted pink" tabindex="0" name="Registered">Register</div>
		</form>
    </div>
    